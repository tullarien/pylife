#training function
def listsum(numList):
    theSum = 0
    for i in numList:
        theSum = theSum + i
    return theSum

print(listsum([2,5,6,7]))

def factorial(n):
    res = 1
    for i in range(1, n + 1):
        res = res * i
    return res
print(factorial(10))

#training recursive function
def fact(n):
    if n == 0:
        return 1 #stop event
    else:
        return n * fact(n - 1) # 10 * 9 , check 0, 9*8,check 0
print(fact(10))

#training fibbonacci example
#F=Fn-1+Fn-2
def fibb(n):
    if n in (1, 2):
        return 1
    else:
        return fibb(n-1) + fibb(n-2)
print(fibb(10))

#training return by index from line ; вернуть второе и пятое число строки чисел
