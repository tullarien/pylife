
#объявим функцию факториала. добавим return для завершения и собственно саму формулу вычисления. получаем число и уменьшаем до единицы, когда равно единице. срабатывает return и функция завершается
def factorial(i):  
   if i == 1:  
       return i  
   else:  
       return i*factorial(i-1)  
#введем собственно число добавим проверки
num = int(input("Enter a number: "))  
if num < 0:  
   print("не должно быть меньше нуля")  
elif num == 0:  
   print("1")  
else:  
   print("Факториал",num,"=",factorial(num))  