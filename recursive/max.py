def Max(s):
    if len(s) == 1:
        return s[0]
    m = Max(s[1:]) #index 
    return m if m > s[0] else s[0]
    #need input event